const { _, log, Pipe } = require('basd')

/**
 * The Drive class provides methods for file storage and operations.
 */
class Drive {
  /**
   * @constructor
   * @param {Object} storage - The underlying storage engine
   * @param {Object} [opts={}] - Options for the Drive instance
   * @param {number} [opts.chunkSize=8192] - The size of file chunks
   * @param {number} [opts.hashLength=8] - The length of hash for chunks
   */
  constructor(storage, opts = {}) {
    _.objProp(this, 'opts', _.defaults(opts, {
      chunkSize: 2 << 13,
      hashLength: 32,
    }))
    _.objProp(this, 'storage', storage)
  }

  /**
   * Builds a sequence of write operations to store a file.
   * @async
   * @param {string} fid - File ID
   * @param {Buffer} buffer - File content as a buffer
   * @param {Array} [ops=[]] - List of operations
   * @param {Object} [chunks={}] - Chunks of the file
   * @returns {Object} An object containing operations and chunks
   */
  async buildWriteFile(fid, buffer, ops = [], chunks = {}) {
    const path = this.storage.sub(fid).path.slice()

    const chunkSize = this.opts.chunkSize
    const numChunks = Math.ceil(buffer.length / chunkSize)
    for (let ii = 0; ii < numChunks; ii++) {
      const hex = _.lexint.pack(ii, 'hex')
      const chunk = buffer.slice(ii * chunkSize, chunkSize * (1 + ii))
      const hash = _.hash(chunk, this.opts.hashLength)
      chunks[hash] = chunk
      const op = { path, type: 'put', key: hex, value: hash }
      ops.push(op)
    }

    return { ops, chunks }
  }

  /**
   * Writes a file to the storage.
   * @async
   * @param {string} fid - File ID
   * @param {Buffer} buffer - File content as a buffer
   */
  async writeFile(fid, buffer) {
    const { ops, chunks } = await this.buildWriteFile(fid, buffer)
    await this.commit(ops, chunks)
  }

  /**
   * Builds a sequence of delete operations to remove a file.
   * @async
   * @param {string} fid - File ID
   * @param {Array} [ops=[]] - List of operations
   * @param {Array} [chunks=[]] - Chunks of the file
   * @returns {Object} An object containing operations and chunks
   */
  async buildDeleteFile(fid, ops = [], chunks = []) {
    const db = this.storage.sub(fid)
    const path = db.path.slice()
    for await (const [key, value] of db.iterator()) {
      const op = { path, type: 'del', key }
      chunks.push(value)
      ops.push(op)
    }
    return { ops, chunks }
  }

  /**
   * Deletes a file from the storage.
   * @async
   * @param {string} fid - File ID
   * @returns {Buffer} The concatenated chunks of the deleted file
   */
  async deleteFile(fid) {
    const { ops, chunks } = await this.buildDeleteFile(fid)
    await this.commit(ops)
    return Buffer.concat(chunks)
  }

  /**
   * Commits operations to the storage.
   * @async
   * @param {Array} ops - List of operations
   * @param {Object} [chunks={}] - Chunks of the file
   */
  async commit(ops, chunks = {}) {
    ops = ops.map(op => ({ ...op, value: chunks[op.value] }))
    await this.storage.root.batch(ops)
  }

  /**
   * Reads a file from the storage.
   * @async
   * @param {string} fid - File ID
   * @returns {Buffer} The content of the file
   */
  async readFile(fid) {
    const chunks = []
    for await (const [key, value] of this.storage.sub(fid).iterator()) {
      chunks.push(value)
    }
    return Buffer.concat(chunks)
  }

  /**
   * Creates a writable stream for a file.
   * @param {string} fid - File ID
   * @param {Object} [opts={}] - Stream options
   * @returns {Object} A writable stream
   */
  createWriteStream(fid, opts = {}) {
    const db = this.storage.sub(fid)
    let ii = 0
    let aggregateBuffer = Buffer.alloc(0)
    const chunkSize = this.opts.chunkSize
    const stream = new Pipe(data => {
      aggregateBuffer = Buffer.concat([aggregateBuffer, data])
      if (aggregateBuffer.length >= chunkSize) {
        const hex = _.lexint.pack(ii++, 'hex')
        const chunk = aggregateBuffer.slice(0, chunkSize)
        aggregateBuffer = aggregateBuffer.slice(chunkSize)
        return db.put(hex, chunk)
      }
    })
    stream.on('finish', () => {
      const hex = _.lexint.pack(ii, 'hex')
      return db.put(hex, aggregateBuffer)
    })
    return stream
  }

  /**
   * Creates a readable stream for a file.
   * @param {string} fid - File ID
   * @param {Object} [opts={}] - Stream options
   * @returns {Object} A readable stream
   */
  createReadStream(fid, opts = {}) {
    const db = this.storage.sub(fid)
    const dbIterator = db.iterator()
    const stream = new Pipe({
      async read () {
        const { value, done } = await dbIterator.next()
        if (done)
          this.push(null)
        else
          this.push(value[1])
      }
    })
    return stream
  }

  /**
   * Gets the path of the storage.
   * @returns {Array} The path of the storage
   */
  get path() {
    return this.storage.path.slice()
  }

  /**
   * Creates a sub-instance of Drive.
   * @param {string|Array} path - Path for the sub-instance
   * @param {Object} [opts] - Options for the sub-instance
   * @returns {Drive} A sub-instance of Drive
   */
  sub(path, opts) {
    const storage = this.storage.sub(path, opts)
    opts = { ...this.opts, ...opts }
    return new this.constructor(storage, opts)
  }
}

module.exports = Drive
