const { _, log } = require('basd')
const MockDb = require('@plaindb/storage/mock')
const Drive = require('../lib/drive')

describe('Drive', () => {
  let storage
  let drive

  const dbPrep = () => {
    storage = new MockDb()
    drive = new Drive(storage, {
      chunkSize: 16,
      hashLength: 8
    })
  }

  describe('writeFile', () => {
    beforeEach(dbPrep)
    it('should write file', async () => {
      const fid = 'file1'
      const buffer = Buffer.from('Hello, world!')
      await drive.writeFile(fid, buffer)
      const content = await drive.readFile(fid)
      expect(content.toString()).to.equal('Hello, world!')
    })
  })

  describe('deleteFile', () => {
    beforeEach(dbPrep)

    it('should delete file', async () => {
      const fid = 'file2'
      const buffer = Buffer.from('Test delete')
      await drive.writeFile(fid, buffer)
      const deletedContent = await drive.deleteFile(fid)

      expect(deletedContent.toString()).to.equal('Test delete')

      try {
        await drive.readFile(fid)
      } catch (error) {
        expect(error.message).to.equal('File not found')
      }
    })

    it('should delete file and should not exist post-deletion', async () => {
      await drive.writeFile('stream.txt', Buffer.from('To be deleted'))
      await drive.deleteFile('stream.txt')

      try {
        await drive.readFile('stream.txt')
      } catch (err) {
        expect(err.message).to.equal('File not found')
      }
    })
  })

  describe('readFile', () => {
    beforeEach(dbPrep)
    it('should read file', async () => {
      const fid = 'file3'
      const buffer = Buffer.from('Read me')
      await drive.writeFile(fid, buffer)

      const content = await drive.readFile(fid)
      expect(content.toString()).to.equal('Read me')
    })
  })

  describe('sub directory', () => {
    beforeEach(dbPrep)
    it('should create sub-directory and write file', async () => {
      const buffer = _.toBuffer('Hello World '.repeat(3))
      const subDrive = drive.sub('child')
      await subDrive.writeFile('hello.txt', buffer)

      const content = await subDrive.readFile('hello.txt')
      expect(content.toString()).to.equal('Hello World Hello World Hello World ')
    })

    it('should create a sub drive', () => {
      beforeEach(dbPrep)
      const subDrive = drive.sub('folder1')
      expect(subDrive.path).to.deep.equal(['folder1'])
    })
  })

  describe('createWriteStream and createReadStream', () => {
    // beforeAll(dbPrep)
    it('should write through a stream', async () => {
      const writer = drive.createWriteStream('stream.txt')
      writer.write(Buffer.from('Hello World'))
      writer.write(Buffer.from(' and '))
      writer.write(Buffer.from('How Are You?'))
      writer.end(); await new Promise(resolve => writer.on('finish', resolve))
      const result = await drive.readFile('stream.txt').then(b => _.isBinary(b) ? b.toString() : b)
      expect(result).to.equal('Hello World and How Are You?')
    })
    it('should read through a stream', async () => {
      const reader = drive.createReadStream('stream.txt')
      let readContent = ''
      reader.on('data', data => {
        readContent += data.toString()
      })
      await new Promise(resolve => reader.on('end', resolve))
      expect(readContent.toString()).to.equal('Hello World and How Are You?')
    })
  })
})
