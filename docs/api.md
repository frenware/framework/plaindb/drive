<a name="Drive"></a>

## Drive
The Drive class provides methods for file storage and operations.

**Kind**: global class  

* [Drive](#Drive)
    * [new Drive(storage, [opts])](#new_Drive_new)
    * [.path](#Drive+path) ⇒ <code>Array</code>
    * [.buildWriteFile(fid, buffer, [ops], [chunks])](#Drive+buildWriteFile) ⇒ <code>Object</code>
    * [.writeFile(fid, buffer)](#Drive+writeFile)
    * [.buildDeleteFile(fid, [ops], [chunks])](#Drive+buildDeleteFile) ⇒ <code>Object</code>
    * [.deleteFile(fid)](#Drive+deleteFile) ⇒ <code>Buffer</code>
    * [.commit(ops, [chunks])](#Drive+commit)
    * [.readFile(fid)](#Drive+readFile) ⇒ <code>Buffer</code>
    * [.createWriteStream(fid, [opts])](#Drive+createWriteStream) ⇒ <code>Object</code>
    * [.createReadStream(fid, [opts])](#Drive+createReadStream) ⇒ <code>Object</code>
    * [.sub(path, [opts])](#Drive+sub) ⇒ [<code>Drive</code>](#Drive)

<a name="new_Drive_new"></a>

### new Drive(storage, [opts])

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| storage | <code>Object</code> |  | The underlying storage engine |
| [opts] | <code>Object</code> | <code>{}</code> | Options for the Drive instance |
| [opts.chunkSize] | <code>number</code> | <code>8192</code> | The size of file chunks |
| [opts.hashLength] | <code>number</code> | <code>8</code> | The length of hash for chunks |

<a name="Drive+path"></a>

### drive.path ⇒ <code>Array</code>
Gets the path of the storage.

**Kind**: instance property of [<code>Drive</code>](#Drive)  
**Returns**: <code>Array</code> - The path of the storage  
<a name="Drive+buildWriteFile"></a>

### drive.buildWriteFile(fid, buffer, [ops], [chunks]) ⇒ <code>Object</code>
Builds a sequence of write operations to store a file.

**Kind**: instance method of [<code>Drive</code>](#Drive)  
**Returns**: <code>Object</code> - An object containing operations and chunks  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| fid | <code>string</code> |  | File ID |
| buffer | <code>Buffer</code> |  | File content as a buffer |
| [ops] | <code>Array</code> | <code>[]</code> | List of operations |
| [chunks] | <code>Object</code> | <code>{}</code> | Chunks of the file |

<a name="Drive+writeFile"></a>

### drive.writeFile(fid, buffer)
Writes a file to the storage.

**Kind**: instance method of [<code>Drive</code>](#Drive)  

| Param | Type | Description |
| --- | --- | --- |
| fid | <code>string</code> | File ID |
| buffer | <code>Buffer</code> | File content as a buffer |

<a name="Drive+buildDeleteFile"></a>

### drive.buildDeleteFile(fid, [ops], [chunks]) ⇒ <code>Object</code>
Builds a sequence of delete operations to remove a file.

**Kind**: instance method of [<code>Drive</code>](#Drive)  
**Returns**: <code>Object</code> - An object containing operations and chunks  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| fid | <code>string</code> |  | File ID |
| [ops] | <code>Array</code> | <code>[]</code> | List of operations |
| [chunks] | <code>Array</code> | <code>[]</code> | Chunks of the file |

<a name="Drive+deleteFile"></a>

### drive.deleteFile(fid) ⇒ <code>Buffer</code>
Deletes a file from the storage.

**Kind**: instance method of [<code>Drive</code>](#Drive)  
**Returns**: <code>Buffer</code> - The concatenated chunks of the deleted file  

| Param | Type | Description |
| --- | --- | --- |
| fid | <code>string</code> | File ID |

<a name="Drive+commit"></a>

### drive.commit(ops, [chunks])
Commits operations to the storage.

**Kind**: instance method of [<code>Drive</code>](#Drive)  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| ops | <code>Array</code> |  | List of operations |
| [chunks] | <code>Object</code> | <code>{}</code> | Chunks of the file |

<a name="Drive+readFile"></a>

### drive.readFile(fid) ⇒ <code>Buffer</code>
Reads a file from the storage.

**Kind**: instance method of [<code>Drive</code>](#Drive)  
**Returns**: <code>Buffer</code> - The content of the file  

| Param | Type | Description |
| --- | --- | --- |
| fid | <code>string</code> | File ID |

<a name="Drive+createWriteStream"></a>

### drive.createWriteStream(fid, [opts]) ⇒ <code>Object</code>
Creates a writable stream for a file.

**Kind**: instance method of [<code>Drive</code>](#Drive)  
**Returns**: <code>Object</code> - A writable stream  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| fid | <code>string</code> |  | File ID |
| [opts] | <code>Object</code> | <code>{}</code> | Stream options |

<a name="Drive+createReadStream"></a>

### drive.createReadStream(fid, [opts]) ⇒ <code>Object</code>
Creates a readable stream for a file.

**Kind**: instance method of [<code>Drive</code>](#Drive)  
**Returns**: <code>Object</code> - A readable stream  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| fid | <code>string</code> |  | File ID |
| [opts] | <code>Object</code> | <code>{}</code> | Stream options |

<a name="Drive+sub"></a>

### drive.sub(path, [opts]) ⇒ [<code>Drive</code>](#Drive)
Creates a sub-instance of Drive.

**Kind**: instance method of [<code>Drive</code>](#Drive)  
**Returns**: [<code>Drive</code>](#Drive) - A sub-instance of Drive  

| Param | Type | Description |
| --- | --- | --- |
| path | <code>string</code> \| <code>Array</code> | Path for the sub-instance |
| [opts] | <code>Object</code> | Options for the sub-instance |

