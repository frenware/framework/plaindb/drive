declare class Drive {
  constructor(storage: any, opts?: {
    chunkSize?: number;
    hashLength?: number;
  });

  buildWriteFile(
    fid: string, 
    buffer: Buffer, 
    ops?: any[], 
    chunks?: Record<string, Buffer>
  ): Promise<{ ops: any[], chunks: Record<string, Buffer> }>;

  writeFile(fid: string, buffer: Buffer): Promise<void>;

  buildDeleteFile(
    fid: string, 
    ops?: any[], 
    chunks?: Buffer[]
  ): Promise<{ ops: any[], chunks: Buffer[] }>;

  deleteFile(fid: string): Promise<Buffer>;

  commit(ops: any[], chunks?: Record<string, Buffer>): Promise<void>;

  readFile(fid: string): Promise<Buffer>;

  createWriteStream(fid: string, opts?: any): Pipe;

  createReadStream(fid: string, opts?: any): Pipe;

  sub(path: any, opts?: any): Drive;

  readonly path: any[];
}
